package com.example.jpa.demo;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FilmRepository extends CrudRepository<FilmEO,Integer>
{
    public List<FilmEO> findByTitleLike(String title);
}
