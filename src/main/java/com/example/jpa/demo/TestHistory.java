package com.example.jpa.demo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * <pre>
 * create table test_history
 * (
 *     createDt datetime     not null,
 *     MTL_NO   varchar(15)  not null,
 *     TITLE    varchar(400) null,
 *     primary key (createDt, MTL_NO)
 * )
 *     comment '복합키테이블';
 *     </pre>
 *   복합키 관련 설정방법 샘플
 * @IdClass에 복합키정의 클래스를 생성한다.
 */
@Entity
@Getter
@Setter
@Table(name="test_history")
@IdClass(TestHistoryId.class)
@ToString
public class TestHistory {

    @Id
    @Column(name ="createdt")
    private LocalDateTime createdt;
    @Id
    @Column(name ="MTL_NO")
    private String mtlNo;
    @Column(name ="TITLE")
    private String title;
}
