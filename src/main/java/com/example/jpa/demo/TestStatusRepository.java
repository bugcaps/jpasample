package com.example.jpa.demo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

public interface TestStatusRepository extends CrudRepository<TestStatus,Integer>
{

}
