package com.example.jpa.demo;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * CrudRepository<Entity클래스, 복합키 구성클래스>
 */
public interface TestHistoryRepository  extends CrudRepository<TestHistory, TestHistoryId>
{
    public List<TestHistory> findByMtlNoIn(List<String> mtlNoList);

}
