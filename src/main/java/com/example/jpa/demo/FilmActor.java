package com.example.jpa.demo;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Getter
@Setter
@IdClass(FilmActorId.class)
@Table(name="film_actor")
public class FilmActor {
    @Id
    @Column(name="actor_id")
    private Integer actorId;
    @Id
    @Column(name="film_id")
    private Integer filmId;





}
