package com.example.jpa.demo;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
public class FilmActorId implements Serializable {
    @EqualsAndHashCode.Include
    @Id
    @Column(name="actor_id")
    private Integer actorId;

    @EqualsAndHashCode.Include
    @Id
    @Column(name="film_ID")
    private Long filmId;

}
