package com.example.jpa.demo;

import org.springframework.data.repository.CrudRepository;
import java.util.List;

public interface ActorRepository extends CrudRepository<Actor,Integer>
{
    public List<Actor> findByFirstNameLike(String name);
}
