package com.example.jpa.demo.key;

import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;


@Getter
public enum MtlStatus
{
    READY("시작","1"),
    DOING("작업중","2"),
    END("종료","3");

    private String desc;
    private String legacyCode;

    MtlStatus(String desc,String legacyCode)
    {
        this.desc =desc;
        this.legacyCode = legacyCode;
    }

    public static MtlStatus ofCode(String code) throws Exception
    {
        return Arrays.stream(MtlStatus.values())
                .filter(v ->v.getLegacyCode().equals(code))
                .findAny()
                .orElseThrow(() -> new Exception("값이 없습니다. " + code));
    }

}
