package com.example.jpa.demo;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Getter
@Setter
@Table(name="film")
public class FilmEO {
    @Id
    @Column(name = "film_id")
    private int filmId;

   private String title;
   private String description;
   private String release_year;
   private Integer language_id;
   private Integer original_language_id;
   private Integer rental_duration;
   private Double rental_rate;
   private int length;
   private Double replacement_cost;
   private String rating;
   private String special_features;
   private LocalDateTime last_update;
}
