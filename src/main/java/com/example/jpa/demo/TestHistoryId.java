package com.example.jpa.demo;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 복합키 정의 샘플
 */
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
public class TestHistoryId implements  Serializable
{
    @EqualsAndHashCode.Include
    @Id
    @Column(name="createdt")
    private LocalDateTime createdt;
    @EqualsAndHashCode.Include
    @Id
    @Column(name="MTL_NO")
    private String mtlNo;

    public TestHistoryId(LocalDateTime createDt, String mtlNo)
    {
        this.createdt= createDt;
        this.mtlNo= mtlNo;
        System.out.println("====================createDt" +createDt + " mtlNo" + mtlNo);
    }

}
