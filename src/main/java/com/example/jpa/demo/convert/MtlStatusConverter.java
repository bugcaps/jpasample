package com.example.jpa.demo.convert;

import com.example.jpa.demo.key.MtlStatus;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import java.util.Objects;

import static com.example.jpa.demo.key.MtlStatus.*;

@Converter
public class MtlStatusConverter implements AttributeConverter<MtlStatus,String>
{
    @Override
    public String convertToDatabaseColumn(MtlStatus mtlStatus)
    {
        if (mtlStatus ==null) return null;
        return mtlStatus.getLegacyCode();
    }

    @Override
    public MtlStatus convertToEntityAttribute(String s) {
        if (!Objects.isNull(s)) {
            try {
                return ofCode(s);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
