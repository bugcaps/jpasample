package com.example.jpa.demo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@ToString
@Table(name="actor")
public class Actor
{
    @Id
    @Column(columnDefinition = "actor_id")
    private int actorId;
    @Column(columnDefinition = "first_name")
    private String firstName;
    @Column(columnDefinition = "last_name")
    private String lastName;
    @UpdateTimestamp
    @Column(columnDefinition = "last_update")
    private LocalDateTime lastUpdate;

    @OneToMany(fetch = FetchType.LAZY, cascade =CascadeType.ALL )
    @JoinColumn(name="actor_id")
    List<FilmActor> filmActorList;

}
