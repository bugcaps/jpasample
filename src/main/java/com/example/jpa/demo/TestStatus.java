package com.example.jpa.demo;

import com.example.jpa.demo.convert.MtlStatusConverter;
import com.example.jpa.demo.key.MtlStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * <pre>
 * create table test_status
 * (
 *     seq    int auto_increment
 *         primary key,
 *     mtl_no varchar(15) null,
 *     status varchar(1)  null
 * );
 * </pre>
 * enum 코드 변환 관련 entity정의
 */
@Entity
@Getter
@Setter
@Table(name="test_status")
public class TestStatus
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="seq")
    private Integer seq;
    @Column(name="mtl_no")
    private String mtlNo;
    /** MtlStatus 값은 enum class이다.
     *MtlStatusConverter  MtlStatus의 db->entity, entity-->db 변환 샘플클래스이다.
     */
    @Convert(converter= MtlStatusConverter.class)
    @Column(name="status")
    private MtlStatus status;

}
