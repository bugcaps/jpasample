package com.example.jpa.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FilmRepostisoryTest {
    @Autowired
   private FilmRepository repository;
    @Test
    public void allRead()
    {
        List<FilmEO>  filmEOS = (List<FilmEO>) repository.findAll();
        filmEOS.forEach(a->{
            System.out.println(a.getFilmId() + "," +a.getTitle() );
        });
    }

    @Test
    public void likeTitle()
    {
        List<FilmEO>  filmEOS = (List<FilmEO>) repository.findByTitleLike("ACE%");
        filmEOS.forEach(a->{
            System.out.println(a.getFilmId() + "," +a.getTitle() );
        });
    }




}
