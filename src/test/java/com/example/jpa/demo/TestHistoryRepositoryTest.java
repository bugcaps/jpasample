package com.example.jpa.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestHistoryRepositoryTest {

    @Autowired
    private TestHistoryRepository testHistoryRepository;

    @Test
    public void save()
    {
        TestHistory new1 = new TestHistory();
        new1.setCreatedt(LocalDateTime.now());
        new1.setMtlNo("SBHB0001 01");
        new1.setTitle("test");
        testHistoryRepository.save(new1);
    }
    @Test
    public void update()
    {
        TestHistory new1 = new TestHistory();
                new1.setCreatedt(   LocalDateTime.of(LocalDate.of(2019,4,6),LocalTime.of(13,54,03)));
                new1.setMtlNo("SBHB0001 01");
                new1.setTitle("test3");
        List<TestHistory> list =testHistoryRepository.findByMtlNoIn(Arrays.asList("SBHB0001 01","SBHB0001 02"));

        list.forEach(t->t.setTitle("test333"));
        testHistoryRepository.saveAll(list);

        TestHistory result =testHistoryRepository.save(new1);
        System.out.println("result " + result.getTitle());
    }

    @Test
    public void read()
    {
        testHistoryRepository.findAll().forEach( his -> System.out.println(his));
    }

}
