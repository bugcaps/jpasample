package com.example.jpa.demo;

import antlr.collections.impl.IntRange;
import com.example.jpa.demo.TestStatusRepository;
import com.example.jpa.demo.key.MtlStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * 항목의 enum을 사용한 코드값 적용 테스트
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestStatusRepositoryTest   {
    @Autowired
    private TestStatusRepository testStatusRepository;

    @Test
    public void read()
    {
        testStatusRepository.findAll().forEach(x -> System.out.println(x.getMtlNo()+"\t" +x.getStatus()));
    }

    @Test
    public void update()
    {

        Iterable<TestStatus>  statusIterable =  testStatusRepository.findAll();
        ArrayList<TestStatus>  statusArrayList = new ArrayList<>();
        statusIterable.forEach(
                  x->{
                      if (x.getStatus() == MtlStatus.READY)
                      {
                          x.setStatus(MtlStatus.DOING);
                          statusArrayList.add(x);
                      }
                  }

        );
        testStatusRepository.saveAll(statusArrayList);
    }
    @Test
    public void insert()
    {
        testStatusRepository.saveAll(insertCount(10));
    }

    private List<TestStatus> insertCount(int cnt)
    {
        List<TestStatus> dataList =new ArrayList<>();
         IntStream.range(0,cnt).forEach( x ->{
                         TestStatus status = new TestStatus();
                        status.setMtlNo("SBHB" + Math.round(Math.random() * 1000));
                        dataList.add(status);
                 }
         );
         return dataList;
    }
}
