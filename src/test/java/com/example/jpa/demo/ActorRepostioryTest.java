package com.example.jpa.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ActorRepostioryTest
{
    @Autowired
    private ActorRepository repo;
    @Test
    public void readpk()
    {
        Optional<Actor> c = repo.findById(1);
        c.ifPresent(actor -> {
                    System.out.println(actor.getFirstName());
                    System.out.println(actor.getLastUpdate());
                    System.out.println(actor.getFilmActorList());
                });

        List<Actor> clist = (List<Actor>) repo.findAll();
        clist.forEach( a -> System.out.println(a.getActorId() + "," +a.getFirstName()));
    }
    @Test
    public void firstName()
    {
        List<Actor> c = repo.findByFirstNameLike("김%");
        c.forEach(actor -> {
                    System.out.println( actor.getActorId()+" "+ actor.getFirstName()  );
                    actor.getFilmActorList().forEach(filmActor -> System.out.println("\t filmActor" + filmActor.getFilmId()));
             }
        );

    }


@Test
    public void update()
    {
        Actor c = repo.findById(1).get();
        c.setFirstName("김영환");
        c.setLastName("test");
        repo.save(c);
    }
  @Test
    public void insert()
    {
        Actor c = new Actor();
        c.setFirstName("김영환");
        c.setLastName("test");
        repo.save(c);

    }

}
//
//
//package com.apress.springrecipes;
//
//        import java.util.stream.Stream;
//
//        import org.junit.Test;
//        import org.junit.runner.RunWith;
//        import org.springframework.beans.factory.annotation.Autowired;
//        import org.springframework.boot.test.context.SpringBootTest;
//        import org.springframework.test.context.junit4.SpringRunner;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//public class BoardRepositoryTests {
//    @Autowired
//    private BoradRepository boardRepo;
//    @Autowired
//    private BoradPagingRepository paging;
//    @Test
//    public void inspect() {
//        Class<?> clz= boardRepo.getClass();
//        System.out.println(clz.getName());
//        Class<?>[] interfaces = clz.getInterfaces();
//
//        Stream.of(interfaces).forEach(inter->System.out.println(inter.getName()));
//
//        Class<?> superClasses =clz.getSuperclass();
//        System.out.println("==========" +superClasses.getName());
//
//    }
//
//    @Test
//    public void testInsert() {
//
//        Board  b = new Board();
//        b.setTitle("게시물 제목");
//        b.setContent("게시물 내용");
//        b.setWriter("user00");
//
//        boardRepo.save(b);
//
//
//    }
//
//    @Test
//    public void testRead() {
//
//        Board  b = new Board();
//        b.setTitle("게시물 제목");
//        b.setContent("게시물 내용");
//        b.setWriter("user00");
//
//        Board c =boardRepo.findById(1L).get();
//        System.out.println("dddddd" + c);
//
//    }
//    @Test
//    public void testPage() {
//
//        Board  b = new Board();
//        b.setTitle("게시물 제목");
//        b.setContent("게시물 내용");
//        b.setWriter("user00");
//
//        Board c =boardRepo.findById(1L).get();
//        System.out.println("dddddd" + c);
//
//    }
//}
